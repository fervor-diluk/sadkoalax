import "@ethersproject/shims";
import { BigNumber } from "ethers";
import { getContractObj } from "./contract";

export async function mint(_mintAmount, provider) {
  console.log("Normal Minting");
  const myContract  = getContractObj(provider);

  try {
    var cost;
    const c = await myContract.cost();
    cost = BigNumber.from(c);
    var tx = await myContract.mint(_mintAmount, {
      value: cost.mul(_mintAmount),
    });
    return tx;
  } catch (error) {
    console.log(error);
    return false;
  }
}


export async function mintWhitelist(
  _mintAmount,
  proof,
  provider
) {
  console.log("DEBUG:", proof);
  const mycontract = getContractObj(provider);
  try {
    var cost;
    const c = await myContract.whitelistCost();
    cost = BigNumber.from(c);

    var tx = await mycontract.mintWhitelist(proof, {
      value: cost.mul(_mintAmount),
    });

    return tx;
  } catch (error) {
    console.log("Error Message --->",error);
    console.log("Error --->",error);
    return false;
  }
}



export async function getAllInfo(provider) {
  const mycontract = getContractObj(provider);
  try {
    const [
      totalSupply,
      maxSupply,
      mintPrice,
      ownerAddress,
      whitelistCost,
      whiteListingSale,
      maxMintAmountPerTransaction,
      paused,
      maxMintAmountPerWallet,
      maxMintAmountPerWhitelist 
     
      
    ] = await Promise.all([
      mycontract.totalSupply(),
      mycontract.maxSupply(),
      mycontract.cost(),
      mycontract.owner(),
      mycontract.whitelistCost(),
      mycontract.whiteListingSale(),
      mycontract.maxMintAmountPerTransaction(),
      mycontract.paused(),
      mycontract.maxMintAmountPerWallet(),
      mycontract.maxMintAmountPerWhitelist()
    ]);

    return {
      totalSupply,
      maxSupply,
      mintPrice,
      ownerAddress,
      whitelistCost,
      whiteListingSale,
      maxMintAmountPerTransaction,
      paused,
      maxMintAmountPerWallet,
      maxMintAmountPerWhitelist 
    };
  } catch (error) {
    console.log(error);
  }
}

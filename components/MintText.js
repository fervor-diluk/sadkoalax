/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import React from 'react'
import Link from 'next/link'
import ConnectModal from "./Connect/ConnectModal";


function MintText() {
    return (
        <div>
            <div className="container">
                <div className="card">
                    <div>

                        <div>
                            <Link href="https://sadkoala.io/">
                                <img
                                    src="/nftimg.jpg"
                                    className="ImgNft"
                                    alt="Openseaimage"
                                    width={200}
                                />
                            </Link>
                        </div>
                        <div className='container'>
                            <h1>Please Connect Your Wallet to Mint SK NFTS</h1>
                            <ConnectModal />

                        </div>


                    </div>

                </div>

            </div>
        </div>
    )
}

export default MintText
/* eslint-disable @next/next/no-img-element */
/* eslint-disable @next/next/link-passhref */
import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { Row, Col, Container, Button, Modal } from "react-bootstrap";
import { useWeb3React } from "@web3-react/core";
import { mint, getAllInfo, mintWhitelist } from "../lib/contractMethods";
import toast from "react-hot-toast";
import axios from "axios";
import { BigNumber, ethers } from "ethers";
import { getContractObj } from "../lib/contract";

function MintFrom() {
  const { account, active, library } = useWeb3React();
  const myContract = getContractObj(library?.getSigner());

  const [qty, setqty] = useState(1);
  const [proof, setProof] = useState([]);
  const [addressBalance, setAddressBalance] = useState(0);
  const [loading, setLoading] = useState(true);
  const [isChimp, setIsChimp] = useState(false);
  const [show, setShow] = useState({
    show: false,
    title: "",
    link: "",
    progress: false,
    dismiss: false,
    buttonText: "",
  });



  const [info, setInfo] = useState({
    totalSupply: 0,
    maxSupply: 0,
    mintPrice: 0,
    ownerAddress: "",
    whitelistCost: 0,
    whiteListingSale: 0,
    maxMintAmountPerTransaction: 0,
    paused: 0,
    maxMintAmountPerWallet: 0,
    maxMintAmountPerWhitelist: 0
  });

  useEffect(() => {
    if (active) {
      console.log("Active getting engine", active);
      console.log("My address", library?.getSigner());

      setTimeout(() => {
        getEngine();
        getMintBalance();
      }, 500);
    }
  }, [active]);

  const handleClose = () => setShow(false);

  async function getEngine() {
    toast("Loading contract.. please wait");
    setLoading(true);
    const data = await getAllInfo(library?.getSigner());
    console.log("Contract info", data);
    setInfo(data);
    if (data.whiteListingSale) {
      console.log("Whitelisting sale eneabled");
      //call the api and set proof
      const proof = await getWhitelistingProof();
      setProof(proof);
      if (proof.length > 0) {
        setIsChimp(true)
      }
      console.log("proof --->", proof)
    }
    setLoading(false);
    toast("Contract loaded, Happy minting!");
  }



  async function getWhitelistingProof() {
    try {
      const response = await axios.get(
        `https://us-central1-doodories.cloudfunctions.net/app/getproofchimps/${account}`
      );
      const whitelistingData = response.data;

      console.log("Whitelisting proof Normal", whitelistingData);
      return whitelistingData.data;
    } catch (error) {
      console.log("Could not reach the server for whitelisting");
    }
  }

  function increment() {
    // console.log(info.whiteListingSale)

    if (info.whiteListingSale) {
      if (proof.length > 0) {
        if (qty < info.maxMintAmountPerWhitelist) {
          setqty(qty + 1);
        }
      }

    } else {

      if (qty < info.maxMintAmountPerWallet) {
        setqty(qty + 1);
      }
    }
  }

  function decrease() {
    if (qty > 1) {
      setqty(qty - 1);
    }
  }

  function showMintModal(state, title, link, progress, dismiss, buttonText) {
    setShow({
      show: state,
      title,
      link,
      progress,
      dismiss,
      buttonText,
    });
  }

  async function getMintBalance() {
    try {
      let isCheck = await myContract.addressMintedBalance(account);

      let getbalance = JSON.parse(BigNumber.from(isCheck));

      setAddressBalance(getbalance);


    } catch (error) {
      console.log("getwhitelist error", error);
      console.log("Opps something went wrong get Whitelisting Mint Balance");
    }
  }

  function saleStatus() {
    if (active) {
      if (info.paused) {
        return "Contract is Paused";
      } else {
        if (info.whiteListingSale) {
          if (proof.length > 0) {
            if (addressBalance == info.maxMintAmountPerWhitelist) {

              return `Your Pre-Sale Quota Exceeded !!!`;
            }
            if (info.totalSupply >= info.maxSupply) {
              return "Sold Out!!!"
            }

          } else {
            return "";
          }
          return "Per-Sale Start";
        } else {
          if (addressBalance == info.maxMintAmountPerWallet) {
            return `Your Public Sale  Quota Exceeded !!!`;
          }
          if (info.totalSupply >= info.maxSupply) {
            return "Sold Out!!!"
          }
          return "Public Sale";
          
        }
      }
    } else {
      return "";
    }
  }

  const btntext = () => {

    if (account) {
        if (info.whiteListingSale) {
            if (addressBalance == info.maxMintAmountPerWhitelist) {
                return "Sold Out!!!"
            }
            return "Mint"
        } else {
            if (addressBalance == info.maxMintAmountPerWallet) {
                return "Sold Out!!!"
            }
            return "Mint"
        }

    } else {
        return "Connect Wallet"
    }
};

  function isWhitelisted() {
    if (active && info.whiteListingSale && !loading) {
      if (proof.length == 0) {
        return "Sorry, your address is not whitelisted";
      } else {
        return "";
      }
    }
  }
  return (
    <div>
      <div className="mintmodalcontainer">
        <Modal show={show.show} onHide={handleClose} className="mymodal">
          <Modal.Body>
            <div className="mintmodal">
              <img
                src="/success.png"
                className="mintmodalimage"
                alt="Mintmodalimage"
              />
              <h2>{show.title}</h2>
              <h3>
                See the transaction on{" "}
                <a href={show.link} target="_blank" rel="noreferrer">
                  Etherscan
                </a>
              </h3>
              {show.progress && (
                <div className="spinner-border text-primary" role="status">
                  <span className="sr-only"></span>
                </div>
              )}
              <h3>{show.body}</h3>

              {show.dismiss && (
                <button
                  className="btn btn-primary btn-round"
                  onClick={handleClose}
                >
                  {show.buttonText}
                </button>
              )}
            </div>
          </Modal.Body>
        </Modal>
      </div>
      <div className="container">
        <div className="card">
          <div>

            <div>
              <Link href="https://sadkoala.io/">
                <img
                  src="/nftimg.jpg"
                  className="ImgNft"
                  alt="Openseaimage"
                  width={200}
                />
              </Link>
            </div>
            <div className='status-container'>
              <div className="presale-badge">{saleStatus()}</div>
              <h1 className='whitelisted-status'>{isWhitelisted()}</h1>

            </div>

            <div className="btngroup">
              <button type="button" className="btn btn-primary" id="nftBtn" onClick={decrease}>
                -
              </button>

              <div className="qtyNFT"> {qty}</div>

              <button type="button" className="btn btn-primary" id="nftBtn" onClick={increment} disabled={isChimp}>
                +
              </button>
            </div>

            <div>
              <button className="btn btn-primary" id="MintBtn"
                onClick={async () => {
                  try {
                    if (!account) {
                      return;
                    }

                    if (info.paused) {
                      return toast.error("Sale is not Active yet");
                    } else if (info.whiteListingSale) {
                      if (proof.length == 0) {
                        return toast.error("Your address is not whitelisted");
                      }
                    }

                    toast("Please wait..", {
                      icon: "👏",
                    });
                    var tx;
                    if (info.whiteListingSale) {
                      tx = await mintWhitelist(
                        qty,
                        proof,
                        library?.getSigner(),
                      );
                    } else {
                      tx = await mint(qty, library?.getSigner());
                    }
                    showMintModal(
                      true,
                      "Mint submitted",
                      `https://etherscan.io/tx/${tx.hash}`,
                      true,
                      false,
                      ""
                    );
                    await tx.wait(1)
                    console.log("tx=====>", tx)
                    showMintModal(
                      true,
                      "Mint Success",
                      `https://etherscan.io/tx/${tx.hash}`,
                      false,
                      true,
                      "Done"
                    );

                  } catch (error) {
                    console.log(typeof error);
                    console.log("Error", error.toString());
                    if (error.toString().includes("execution reverted")) {
                      toast.error("Please contact Admins");
                    } else {
                      toast.error("Insufficient funds or Transaction Error");
                    }

                    showMintModal(false, "", "", false, true, "Close");
                  }
                }}
                disabled={!account && !loading}
              >
                {btntext()}
              </button>
            </div>

            <div className="note">
              <h3>Guide</h3>
              <p>
                Connect Wallet via Metamask Choose Desired Quantity Click Mint Now. <br />
                Ensure you have sufficient ETH in gas.
              </p>
              <h3 >NOTE</h3>
              <p >
                Adjust your gas fees accordingly for your transaction to be priortized. (For reference:{' '}
                <a href="https://ethgasstation.info">
                  https://ethgasstation.info
                </a>
                )
                <br />
                <br />
                NFT will be revealed 96hrs after minting event. (Follow our socials for exact time)
              </p>
            </div>

          </div>

        </div>

      </div>
    </div>
  )
}

export default MintFrom
import { useWallet } from '../lib/hooks/wallet';
import MintForm from '../components/MintFrom';
import MintText from '../components/MintText';
import { Toaster } from "react-hot-toast";
import { useEagerConnect, useInactiveListener } from "../lib/hooks/web3Hook";

export default function Home() {

  const { active } = useWallet();
  // handle logic to eagerly connect to the injected ethereum provider, if it exists and has granted access already
  const triedEager = useEagerConnect();

  // handle logic to connect in reaction to certain events on the injected ethereum provider, if it exists
  useInactiveListener(!triedEager);
  return (
    <>
     <Toaster position="top-center" reverseOrder={false} />
      {active ? (

        <MintForm />

      ) : (

        <MintText />
      )}


    </>
  )
}

import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" integrity="undefined" crossOrigin="anonymous"/>
{/* <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="undefined" crossOrigin="anonymous"></script> */}

          <link href="'https://fonts.googleapis.com/css2?family=East+Sea+Dokdo&family=Julee&family=Nerko+One&family=Rajdhani:wght@300&family=Yanone+Kaffeesatz&display=swap'" rel="stylesheet" />
          <link href="https://fonts.googleapis.com/css2?family=Nerko+One&display=swap" rel="stylesheet" />
          <link
             async
             rel="stylesheet"
             href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
       />          
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
        
      </Html>
    )
  }
}


export default MyDocument